package Wx::Scintilla::TextCtrl;

use 5.008;
use strict;
use warnings;

our $VERSION = '0.34';
our @ISA     = 'Wx::ScintillaTextCtrl';

1;
