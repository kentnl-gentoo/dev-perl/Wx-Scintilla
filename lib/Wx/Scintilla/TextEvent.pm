package Wx::Scintilla::TextEvent;

use 5.008;
use strict;
use warnings;

our $VERSION = '0.34';
our @ISA     = 'Wx::ScintillaTextEvent';

1;
